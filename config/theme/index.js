import { extendTheme, theme as base } from '@chakra-ui/react';

// 2. Call `extendTheme` and pass your custom values
const theme = extendTheme({
  fonts: {
    heading: `'Poppins', sans-serif`,
    body: `'Roboto', sans-serif`,
  },
  breakpoints: {
    xs: '10em',
    sm: '30em',
    md: '48em',
    lg: '62em',
    xl: '80em',
    '2xl': '96em',
    '4xl': '118em',
  },
  colors: {
    blueChill: {
      500: '#4B8F8C',
    },
    green50: {
      500: '#CBD5E0',
    },
    redSeat: {
      700: '#7A0147',
      400: '#D3007A',
    },
    blackSeat: {
      500: '#161C2D',
    }
  }
});

export default theme;
