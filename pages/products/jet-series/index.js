
import { Box, Text, Grid, Heading, Flex } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { FiChevronRight } from 'react-icons/fi';
import { Button, Icon, Image, UnorderedList, Link, ListItem } from '@chakra-ui/react';

export default function AboutUs() {
    const data =
    {
        title: "JET Series",
        image: "/product/jet/jet-yellow.png",
        description: "Incorporate the in-line assembly method. The motor is protected from the corrosive flow and the weather inside the cone. With vertical discharge, these fans are usually roof mounted. Casing and Impeller from Plastic material (Polypropylene).",
        url: 'https://www.seat-ventilation.com/products/by-category/jet-series/',
        page: 'products/jet-series',
        products: [
            { name: 'JET 20', url: 'https://www.seat-ventilation.com/products/by-category/jet-series/jet-20' },
            { name: 'JET 25', url: 'https://www.seat-ventilation.com/products/by-category/jet-series/jet-25' },
            { name: 'JET 30', url: 'https://www.seat-ventilation.com/products/by-category/jet-series/jet-30' },

        ]
    };

    return (
        <Box  >
            <Grid
                mx='12vw' mb='6rem'
                templateColumns={{ base: "1fr", lg: "1fr 1fr" }}
                gap={{ base: "4", md: "6rem" }}
                padding="4"

            >
                {/* First Column: Google Maps Widget */}
                <Box>
                    <Image
                        src={data.image}
                        alt={'SEAT Series'}
                        w={'360px'}
                        my='20px'
                        transition="width 0.5s"
                        _hover={{ opacity: 0.9 }}
                    />
                </Box>

                {/* Second Column: Address and Contact Info */}
                <Box margin="auto">
                    <Heading as='h6' size='lg' textColor='redSeat.700' mb='24px'>
                        {data.title}
                    </Heading>
                    <Text textAlign={'justify'} size={'lg'} textColor=''>{data.description}</Text>
                    <Box mb='10px' />
                    <Box>
                        <UnorderedList key={data.title}>
                            {data.products.map((item) => (
                                <Link target='_blank' key={item.name} href={item.url} _hover={{ color: "redSeat.400" }}>
                                    <ListItem key={item.name}>{item.name}</ListItem>
                                </Link>
                            ))}
                        </UnorderedList>
                    </Box>
                    <Box mb='30px' />
                    <Flex mt="auto">
                        <Button
                            size={'lg'}
                            variant="outline"
                            color='redSeat.700'
                            borderColor="redSeat.700"
                            bg="transparent"
                            onClick={() => { handleButtonClick(data.url) }}
                            _hover={{ bg: "redSeat.700", color: "white" }}
                            rightIcon={<Icon as={FiChevronRight} />}
                        >
                            See Detail
                        </Button>
                    </Flex>
                </Box >
            </Grid >

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box >
    )


}

const handleButtonClick = (url) => {
    import('react-dom').then((ReactDOM) => {
        window.open(url, '_blank');
    });
};
