
import { Box, Text, Grid, Heading, Flex } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { FiChevronRight } from 'react-icons/fi';
import { Button, Icon, Image, UnorderedList, Link, ListItem } from '@chakra-ui/react';

export default function AboutUs() {
    const data =
    {
        title: "SEAT Series",
        image: "/product/seat/seat-green.png",
        description: "Made of polypropylene, a corrosion resistant material. Used in laboratories, Fume Hood (Lemari Asam), and most industrial extractions. Simple to install, small in size and contain a good flow/pressure ratio.",
        url: 'https://www.seat-ventilation.com/products/by-category/seat-series/',
        page: 'products/seat-series',
        products: [
            { name: 'SEAT 15', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-15' },
            { name: 'SEAT 20', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-20' },
            { name: 'SEAT 25', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-25' },
            { name: 'SEAT 30', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-30' },
            { name: 'SEAT 35', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-35' },
            { name: 'SEAT 50', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-50' },
        ]
    };

    return (
        <Box  >
            <Grid
                mx='12vw' mb='6rem'
                templateColumns={{ base: "1fr", lg: "1fr 1fr" }}
                gap={{ base: "4", md: "6rem" }}
                padding="4"

            >
                {/* First Column: Google Maps Widget */}
                <Box>
                    <Image
                        src={data.image}
                        alt={'SEAT Series'}
                        w={'360px'}
                        my='20px'
                        transition="width 0.5s"
                        _hover={{ opacity: 0.9 }}
                    />
                </Box>

                {/* Second Column: Address and Contact Info */}
                <Box margin="auto">
                    <Heading as='h6' size='lg' textColor='redSeat.700' mb='24px'>
                        {data.title}
                    </Heading>
                    <Text textAlign={'justify'} size={'lg'} textColor=''>{data.description}</Text>
                    <Box mb='10px' />
                    <Box>
                        <UnorderedList key={data.title}>
                            {data.products.map((item) => (
                                <Link target='_blank' key={item.name} href={item.url} _hover={{ color: "redSeat.400" }}>
                                    <ListItem key={item.name}>{item.name}</ListItem>
                                </Link>
                            ))}
                        </UnorderedList>
                    </Box>
                    <Box mb='30px' />
                    <Flex mt="auto">
                        <Button
                            size={'lg'}
                            variant="outline"
                            color='redSeat.700'
                            borderColor="redSeat.700"
                            bg="transparent"
                            onClick={() => { handleButtonClick(data.url) }}
                            _hover={{ bg: "redSeat.700", color: "white" }}
                            rightIcon={<Icon as={FiChevronRight} />}
                        >
                            See Detail
                        </Button>
                    </Flex>
                </Box >
            </Grid >

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box >
    )


}

const handleButtonClick = (url) => {
    import('react-dom').then((ReactDOM) => {
        window.open(url, '_blank');
    });
};
