
import { Box, Text, Grid, Heading, Flex } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { FiChevronRight } from 'react-icons/fi';
import { Button, Icon, Image, UnorderedList, Link, ListItem } from '@chakra-ui/react';

export default function AboutUs() {
    const data =
    {
        title: "STORM Series",
        image: "/product/storm/storm-pink.png",
        description: "High pressure fans for applications such as storage cabinets, exhaust arms, washing towers, collectors, Fume Hood (Lemari Asam), or filter boxes requiring high static pressure, and with high pressure drops.",
        url: 'https://www.seat-ventilation.com/products/by-category/storm-series/',
        page: 'products/storm-series',
        products: [
            { name: 'STORM 10', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-10' },
            { name: 'STORM 12', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-12' },
            { name: 'STORM 14', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-14' },
            { name: 'STORM 16', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-16' },
            { name: 'STORM 18', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-18' },
        ]
    };

    return (
        <Box  >
            <Grid
                mx='12vw' mb='6rem'
                templateColumns={{ base: "1fr", lg: "1fr 1fr" }}
                gap={{ base: "4", md: "6rem" }}
                padding="4"

            >
                {/* First Column: Google Maps Widget */}
                <Box>
                    <Image
                        src={data.image}
                        alt={'SEAT Series'}
                        w={'360px'}
                        my='20px'
                        transition="width 0.5s"
                        _hover={{ opacity: 0.9 }}
                    />
                </Box>

                {/* Second Column: Address and Contact Info */}
                <Box margin="auto">
                    <Heading as='h6' size='lg' textColor='redSeat.700' mb='24px'>
                        {data.title}
                    </Heading>
                    <Text textAlign={'justify'} size={'lg'} textColor=''>{data.description}</Text>
                    <Box mb='10px' />
                    <Box>
                        <UnorderedList key={data.title}>
                            {data.products.map((item) => (
                                <Link target='_blank' key={item.name} href={item.url} _hover={{ color: "redSeat.400" }}>
                                    <ListItem key={item.name}>{item.name}</ListItem>
                                </Link>
                            ))}
                        </UnorderedList>
                    </Box>
                    <Box mb='30px' />
                    <Flex mt="auto">
                        <Button
                            size={'lg'}
                            variant="outline"
                            color='redSeat.700'
                            borderColor="redSeat.700"
                            bg="transparent"
                            onClick={() => { handleButtonClick(data.url) }}
                            _hover={{ bg: "redSeat.700", color: "white" }}
                            rightIcon={<Icon as={FiChevronRight} />}
                        >
                            See Detail
                        </Button>
                    </Flex>
                </Box >
            </Grid >

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box >
    )


}

const handleButtonClick = (url) => {
    import('react-dom').then((ReactDOM) => {
        window.open(url, '_blank');
    });
};
