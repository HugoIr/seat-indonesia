
import { Box, Text, Heading, Center } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { Image } from '@chakra-ui/react';

export default function ChemicalIndustry() {


    return (
        <Box >
            <Center mt={'16px'}>
                <Image borderRadius={10} justifyContent={'center'} alt='Chemical Industry' src={'chemical-industry.png'} objectFit={'cover'} w={'80%'} height={'240px'} />
            </Center>
            <Box my='2rem' mx='10vw'>
                <Heading as='h3' textColor='' mb='16px'>
                    Pharmaceutical and chemical industry
                </Heading>
                <Heading as='h6' size='lg' fontWeight={'w400'} textColor='redSeat.700' mb='24px'>
                    Complete range for the air extraction system for pharmaceutical industry
                </Heading>

                <Text textAlign={'justify'} size={'lg'} textColor=''>In the pharmaceutical industry SEAT Ventilation fans are regularly an element of the clean rooms HVAC system.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>Clean room performance and compliance is directly related to the ventilation. The clean room plays a central role in the pharmaceutical industry, in the hospital sector, in agri-food chains or in the space and microelectronics industries.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor='' fontWeight={'600'}>The SEAT Ventilation fans can be perfectly installed in this type of application, after a very rigorous selection.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>This structure is unavoidable when very small particles can affect the products handled or manufactured knowing that many particles invisible to the naked eye present a real danger.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>All thresholds of contamination are daily controlled since delicate components are handled in pharmaceutical laboratories.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>All clean rooms are equipped with an air handling unit, and the exhaust fans are a part of.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>The centrifugal exhaust fan, ensures the flow / pressure fonction of the blown air.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>The development and quality control of new products is often done in laboratories . This involves handling chemicals and substances that can be harmful to the health of users. Safety in these environments is essential.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>Fume hoods are used to protect laboratory workers from any work-related incidents (e.g. spraying, re-dumping of toxic fumes...) and to control exposure of other users to hazardous chemicals.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>The operating principle is to create a laminar flow (dynamic barrier) in front of the fume cupboard of 100fpm (ANSI/AIHA Z9.5 and ASHRAE regulations), in order to let nothing come out of the fume cupboard, bring the pollutants into the extraction network, and evacuate them outside the building.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>Fan selection is determined by the airflow and pressure required to perform the task as efficiently as possible. </Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>To quantify this required pressure, calculate the pressure drop in the system and the pressure drop created by the hood.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>Finally, ensure that the laminar flow, at the front, always complies with the current regulations (ANSI/AIHA Z9.5 and ASHRAE), with a minimum velocity of 100fpm.</Text><br />
            </Box>

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
