
import { Box, Text, Heading, Center } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { Image } from '@chakra-ui/react';

export default function HospitalSolutions() {


    return (
        <Box >
            <Center mt={'16px'}>
                {/* Source Image: https://rs.ui.ac.id/umum/layanan/laboratorium */}
                <Image borderRadius={10} justifyContent={'center'} alt='Hospital Solutions' src={'hospital-solutions.jpg'} objectFit={'cover'} w={'80%'} height={'240px'} />
            </Center>
            <Box my='2rem' mx='10vw'>
                <Heading as='h3' textColor='' mb='16px'>
                    Hospital Solutions
                </Heading>
                <Heading as='h6' size='lg' fontWeight={'w400'} textColor='redSeat.700' mb='24px'>
                    A complete range for the air extraction system for hospitals
                </Heading>

                <Text textAlign={'justify'} fontSize={'lg'} textColor='' fontWeight={'600'}>SEAT Ventilation provides corrosion resistant fans adapted for the extraction of dangerous products handled in hospital laboratories.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>In hospitals, the air quality deteriorates due to the presence of particles of all kinds in the building. In this context, the installation of an air extraction system is essential. Ventilation in hospitals is therefore an essential link in these places of care, especially at this time when Covid 19 is in everyone&apos;s mind.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>This is the guarantee of a good working environment for the establishment&apos;s employees, patients and visitors. Medical personnel remain exposed on a daily basis to gases and fumes that pose health risks.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>In the spaces that require the highest level of cleanliness, air flow management, room air diffusion, and stability of environmental conditions are strictly important</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>The techniques used in pathological anatomy laboratories are characterized by the manipulation of large quantities of hazardous chemicals, including formalin, a gas that is very irritating to the eyes and the respiratory system.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>Manipulation of formaldehyde exposes medical-technical personnel to a health risk by inhalation.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>Considering present regulations requirements, the hospital must set up a chemical risk assessment and prevention procedure, so all laboratories must be equipped with anti-corrosion exhaust fans of toxic gases and vapors.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>Our expertise and in-depth understanding of the medical industry, over 55 years in the ventilation business, has helped deliver the right ventilation solutions to hospitals around the world.</Text><br />

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>SEAT Ventilation produces corrosion-resistant fans and blowers. Great for clinics and hospitals applications, to have an optimal extraction system and to maintain acceptable working conditions in laboratories, patient rooms, care units or operating theater.</Text><br />
            </Box>

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
