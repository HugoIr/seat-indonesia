
import { Box, Text, Heading, Center } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { Image } from '@chakra-ui/react';

export default function ChemicalLaboratories() {


    return (
        <Box >
            {/* <HeroAboutUs /> */}
            <Center mt={'16px'}>
                <Image borderRadius={10} justifyContent={'center'} alt='Chemical laboratories' src={'chemical-lab.jpeg'} objectFit={'cover'} w={'80%'} height={'240px'} />
            </Center>
            <Box my='2rem' mx='10vw'>
                <Heading as='h3' textColor='' mb='16px'>
                    Chemical laboratories of national research centers and universities
                </Heading>
                <Heading as='h6' size='lg' fontWeight={'w400'} textColor='redSeat.700' mb='24px'>
                    A complete range for the air extraction system of chemical laboratories of national research centers and universities
                </Heading>
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>
                    The chemistry lab is a workspace where a wide variety of chemicals are used. All present risks to the health of laboratory workers to varying degrees; this is the reason why we generally use loudspeakers of ventilated protection (fume cupboards) for experiments using large volumes of chemicals whose toxicity is well known.
                </Text>
                <br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>
                    The risk of accidents in a chemical laboratory is very high given the diversity and the large number of operations carried out, and the wide variety of chemicals used or stored.
                </Text>
                <br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>
                    The development of a taste for research begins in schools and universities.
                </Text>

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>Teaching and research laboratories are special work environments because of the diversity and rotation of professionals and users who work in them.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>In addition to the presence of various risk factors, they are subject to a higher frequency of accidents if safe practices are not adopted.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>To ensure the safety of students when handling products, one of the main security devices in a laboratory is the fume hood.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>The fume hood is a ventilated enclosure in which gases, vapors, and fumes are contained. An exhaust fan located outside the building draws air and airborne contaminants through ducts connected to the fume hood and exhausts them into the atmosphere.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>Properly designed, installed, and maintained, the system can offer a substantial degree of protection to the user, provided that its limitations are known and used in accordance with the standards.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>SEAT Ventilation polypropylene blowers draw fumes, vapors, and airborne contaminants into the fume hood or extraction arm and exhaust them outside the building.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>A chemistry laboratory in a research center or university in which operations are carried out with products giving off vapors and corrosive gases, must be equipped with a ventilation system to capture them to maintain continously a safe atmosphere and in the same time protect the health of people working in the laboratory.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>The health of a laboratory can be ensured by a general ventilation system that performs the renewal of the air.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>When operations are carried out on hazardous gaseous or volatile products or reactions which give rise to such products, the use of a fume cupboard is essential: in general, laboratories contain several, depending on their type of activity.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>The fume cupboards are intended to protect the manipulator and its surrounding environment through an extraction system to the outside that sucks corrosive or toxic gases and vapors.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>Constructed with polypropylene, SEAT Ventilation fans are ideally suited for corrosive or toxic work environments.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>In industry applications the laboratories are often used to carry out tests that are hazardous, often contain explosion risks and involve toxic materials. SEAT Ventilation range includes different material options to suit actual needs.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>In laboratories with explosion risk, SEAT Ventilation supplies system with ATEX compliant components.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>SEAT Ventilation blowers can be installed outside the building (on the roof or on the side of the building), but also inside, in galleries or technical rooms.</Text><br />
            </Box><br />

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
