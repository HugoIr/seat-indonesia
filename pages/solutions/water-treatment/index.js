
import { Box, Text, Heading, Center } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { Image } from '@chakra-ui/react';

export default function WaterTreatment() {


    return (
        <Box >
            <Center mt={'16px'}>
                {/* Source Image: https://hydromarque.com/articles/the-role-of-water-treatment-in-sustainability-and-environmental-stewardship */}
                <Image borderRadius={10} justifyContent={'center'} alt='Water Treatment' src={'water-treatment.jpeg'} objectFit={'cover'} w={'80%'} height={'240px'} />
            </Center>
            <Box my='2rem' mx='10vw'>
                <Heading as='h3' textColor='' mb='16px'>
                    Water Treatment
                </Heading>
                <Heading as='h6' size='lg' fontWeight={'w400'} textColor='redSeat.700' mb='24px'>
                    A complete range for the air extraction system for the water treatment plant.
                </Heading>

                <Box alignSelf='left' p={5} borderRadius={8} bgColor={'teal.100'} width={'80%'}>
                    <Text textAlign={'justify'} size={'lg'} textColor=''>SEAT Ventilation extractors are suitable for the ventilation of chlorine drum stores, aeration tank and filtration tank. Chlorine is a highly corrosive gas that can deteriorate all kinds of material like steel (including stainless steel 316L) and can endanger both users and equipment.</Text>
                </Box>
                <br />
                <Text textAlign={'justify'} size={'lg'} textColor=''>SEAT Ventilation anti-corrosion fans are perfectlly adapted for the water treatment plant.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>A water treatment plant ensure that toxic gases and vapours or heat losses can not spread in the atmosphere.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor='' fontWeight={'600'}>SEAT Ventilation full range of fans extract the emitted toxic gases in such a way as to provide operating personnel with a non-hazardous environment and to ensure the protection of equipment and works. </Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>The various physical-chemical processes used in wastewater treatment plants to treat sludge release various toxic gases and fumes.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>These fumes are harmful to the employees as well as attack the structure of the mechanical equipment and buildings.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>Due to wastewater treatment, the ventilation system must constantly extract hazardous substances, fumes and chemical gases such as methane and ammonia . This extraction must take place, in particular, in areas where there is a risk of explosion due to the storage of chlorine drums.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>In some cases, to protect the health and safety of employees, the extraction of hazardous substances with an XP compliant system is strongly recommended. We strongly recommend to check the local regulation.</Text><br />
                <Text textAlign={'justify'} size={'lg'} textColor=''>Its purpose is to ensure that toxic gases and vapours or heat losses can not spread in the atmosphere.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>Seat Ventilation full range of fans extract the emitted toxic gases in such a way as to provide operating personnel with a non-hazardous environment and to ensure the protection of equipment and works. Here below you can find a schematic explaining an installation integrating the anti-corrosion exhaust blower in a water treatment plant.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>To reduce or eliminate the risks due to the presence of various corrosive gases and vapors in water treatment plants, it is imperative to install an extraction system operating effectively 24 hours a day.</Text><br />

                <Text textAlign={'justify'} size={'lg'} textColor=''>SEAT Ventilation extractors made of polypropylene have all the required properties. Polypropylene offers superior durability to most other materials and is resistant to a wide range of chemicals.</Text><br />
            </Box>

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
