
import { Box, Text, Heading, Center } from '@chakra-ui/layout';
import Footer from '../../../components/Footer';
import { Image } from '@chakra-ui/react';

export default function HospitalSolutions() {


    return (
        <Box >
            <Center mt={'16px'}>
                {/* Source Image: https://rs.ui.ac.id/umum/layanan/laboratorium */}
                <Image borderRadius={10} justifyContent={'center'} alt='Fumehood' src={'fumehood.jpeg'} objectFit={'cover'} w={'80%'} height={'240px'} />
            </Center>
            <Box my='2rem' mx='10vw'>
                <Heading as='h3' textColor='' mb='16px'>
                    Solution for Fume Hood (Lemari Asam) Maker
                </Heading>
                <Heading as='h6' size='lg' fontWeight={'w400'} textColor='redSeat.700' mb='24px'>
                    A complete range for the air extraction system for Fume Hood (Lemari Asam)
                </Heading>

                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>SEAT Ventilation fans with high quality material are ideally suited for corrosive or toxic work environments.</Text><br />
                <Text textAlign={'justify'} fontSize={'lg'} textColor=''>The correct choice to fume hood maker. To bring out corrosive air from inside the building area to atmosphere.</Text><br />

            </Box>

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
