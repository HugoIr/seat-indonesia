import { Box } from '@chakra-ui/layout';
import Footer from '../components/Footer';
import HeroHome from '../components/Hero/Home';
import SectionProduct from '../components/SectionProduct';

export default function Home() {


  return (
    <Box h={'200vh'}>
      <HeroHome />
      <SectionProduct />
      <Footer />
    </Box>

  )

}
