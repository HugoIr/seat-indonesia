import { ChakraProvider } from '@chakra-ui/react';
import Layout from '../components/Layout';
import theme from '../config/theme';
import '../styles/globals.css';
import { Metadata } from 'next';
import Head from 'next/head'; // Import Head component from next/head

export const metadata: Metadata = {
  title: "SEAT Ventilation Indonesia",
  description: "SEAT Ventilation INDONESIA, We are a sole agent of SEAT, STORM, JET Series in Indonesian market. SEAT Products are High quality. SEAT Products are competitive price.",
  keywords: ['SEAT Ventilation Indonesia', 'seat ventilation indonesia', 'ventilation', 'kipas pabrik', 'kipas pabrik plastik', 'kipas plastik pabrik', 'kipas pabrik', 'fume hood', 'kipas fume hood', 'fan lemari asam', 'blower lemari asam', 'blower fume hood', 'ventilation fume hood', 'centrifugal fan lemari asam', 'centrifugal fan polyproppyelene', 'blower polypropyelene', 'kipas angin polypropeylene', 'blower anti asam', 'kipas angin tahan asam', 'indonesia'],
  creator: 'SEAT Indonesia',
}


function MyApp({ Component, pageProps }: { Component: React.ComponentType, pageProps: any }) {

  return (
    <ChakraProvider theme={theme}>
      <Head>
        {/* Render metadata */}
        <title>SEAT Ventilation Indonesia</title>
        <meta name="description" content='SEAT Ventilation INDONESIA, We are a sole agent of SEAT, STORM, JET Series in Indonesian market. SEAT Products are High quality. SEAT Products are competitive price.' />
        <meta name="keywords" content="SEAT Ventilation,seat ventilation indonesia,ventilation,kipas pabrik,kipas pabrik plastik,kipas plastik pabrik,centrifugal fan plastik indonesia,centrifugal fan plastik jakarta,kipas fume hood,fan lemari asam,blower lemari asam,blower fume hood,ventilation fume hood,centrifugal fan lemari asam,centrifugal fan polyproppyelene,blower polypropyelene,kipas angin polypropeylene,blower anti asam,kipas angin tahan asam,indonesia" />
        <meta name="author" content="SEAT Ventilation Indonesia" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        {/* Add other metadata as needed */}
      </Head>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ChakraProvider>
  )
}



export default MyApp


