
import { Heading, Center } from '@chakra-ui/layout';
import Footer from '../../components/Footer';
import { Box, Image, Flex, SimpleGrid, Text, useDisclosure, Modal, ModalOverlay, ModalContent, ModalBody, ModalCloseButton } from "@chakra-ui/react";
import { useState } from "react";

export default function Gallery() {
    const { isOpen, onOpen, onClose } = useDisclosure();

    const [selectedImage, setSelectedImage] = useState(null);

    const images = [
        { title: "", src: "/gallery/photo-1.jpg" },
        { title: "", src: "/gallery/photo-2.jpg" },
        { title: "", src: "/gallery/photo-3.jpg" },

    ];
    const handleImageClick = (image) => {
        setSelectedImage(image);
        onOpen();
    };

    return (
        <Box >
            <Box my='2rem' mx='10vw'>
                <Heading as='h5' size='xl' textColor='' mb='16px'>
                    Gallery
                </Heading>
                <Heading as='h6' size='md' fontSize='24px' fontWeight={'w400'} textColor='redSeat.700' mb='24px'>
                    Showcase for our works and products. Available Centrifugal fan Polypropyelene / Plastic sizes :  6 , 8 , 10 , 12 , 14, 20 inch
                </Heading>
            </Box>

            <Box pb='30px' />

            <Box px='14vw'>
                <SimpleGrid columns={{ base: 1, md: 1 }} spacingX={24} spacingY={20}>
                    {images.map((image, index) => (
                        <Box key={index} textAlign="center" onClick={() => handleImageClick(image)} cursor="pointer">

                            <Center>
                                <Image
                                    src={image.src}
                                    alt={image.title}
                                    border={'none'}
                                    radius={8}
                                    maxHeight='360px'
                                />
                            </Center>
                        </Box>
                    ))}
                </SimpleGrid>

                <Modal isOpen={isOpen} onClose={onClose} size={{ md: '4xl', sm: 'xl' }}>
                    <ModalOverlay />
                    <ModalContent >
                        {/* <ModalCloseButton /> */}

                        <ModalCloseButton position="absolute" bgColor={'white'} p={2} color={'gray.500'} size={24} top={-12} right={'-2px'} />

                        <ModalBody p={2} m={0} >
                            {selectedImage && (
                                <Image src={selectedImage.src} alt={selectedImage.title} width="100%" />
                            )}
                        </ModalBody>
                    </ModalContent>
                </Modal>
            </Box>

            <Box pb='10vh' />

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
