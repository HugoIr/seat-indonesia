
import React, { useRef, useEffect, useState } from 'react';

import { Box, Text, } from '@chakra-ui/layout';
import { Icon } from '@chakra-ui/react';
import { keyframes } from '@emotion/react';
import ContactUs from '../../components/ContactUs';
import Footer from '../../components/Footer';
import { FaArrowRight } from 'react-icons/fa';
import HeroAboutUs from '../../components/Hero/AboutUs';
import AnimationLineWithIcon from '../../components/AnimationLineWithIcon';

export default function AboutUs() {

    return (
        <Box >
            <HeroAboutUs />
            <Box my='6rem' mx='12vw'>
                <Box>
                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>PT. Ranescho Sukses Mandiri appointed SEAT Ventilation become Sole agent in Indonesia.
                    </Text>
                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        With our experience more than 20 years for market PP Centrifugal fan and Chemical Industries and supported by
                    </Text>

                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        an expert sales team and professional staff
                    </Text>
                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        We believe can success bring SEAT Ventilation  grow up together with Our company in Indonesia market
                        SEAT Ventilation also come with competitive prices, good quality, full ranges for Fume Hood (Lemari Asam), Water Treatment Plant, Chemical Air, Explosion Proof Area, and others.
                    </Text>
                </Box>
                <Box mb='20px' />

                <AnimationLineWithIcon />
                <Box mb='20px' />

                <Box>
                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        PT Ranescho Sukses Mandiri adalah perusahaan yang bergerak dibidang Fan, Blower atau  Ventilasi dengan memiliki pengalaman lebih dari 20 tahun dalam menjual product tersebut.
                    </Text>
                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        Kami ditunjuk oleh SEAT Ventilations dari Perancis sebagai agent tunggal untuk penjualan product Centrifugal Polypropylene Fan / Centrifugal Plactic Fan di Indonesia.
                    </Text>

                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        Centrifugal Polypropylene Fan / Centrifugal Plactic Fan, dengan material Polypropylene/Plastic sangat cocok dipergunakan untuk udara Asam / Corrosive air atau udara di area Explosion atau Harzardoze area.
                    </Text>
                    <Text textAlign={'center'} fontSize={'lg'} textColor=''>
                        Oleh karena itu kami banyak memiliki pelanggan dari perusahaan peralatan lab yang membuat : Fume hood ( Lemari Asam ) , Perusahaan kimia atau Petrochemical Industri.
                    </Text>
                </Box>
            </Box>


            <ContactUs />
            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
