
import { Box, Text, Heading, Stack, HStack, UnorderedList, ListItem, Flex } from '@chakra-ui/layout';
import { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Image, useBreakpointValue } from '@chakra-ui/react';
import Footer from '../../components/Footer';
import { FaQuestionCircle } from "react-icons/fa";

export default function FAQ() {
    const stackDirection = useBreakpointValue({ base: "column", md: "row" });

    return (
        <Box >
            <Box my='2rem' mx='10vw'>
                <Heading as='h3' textColor='' mb='16px'>
                    Frequently Asked Question (F.A.Q)
                </Heading>
                <Box mb='30px' />
                <Accordion allowMultiple>
                    {/* Accordion 1 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>Flow rate is much lower than expected?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>Check the direction of rotation of the motor, it must rotate in the direction of the arrow on the volute. When the motor rotates in the opposite direction, the air flow is in the right direction but with a very low flow rate.</Text>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>To reverse the direction of rotation of a three-phase asynchronous motor, simply reverse 2 wires on the motor connection:</Text>
                            <Box mb='30px' />
                            <Stack justifyContent={{ xs: 'center', md: 'left' }} direction={stackDirection} spacing={{ xs: 10, md: 40 }}>
                                <Image alt='faq-1-1' src={'/faq/faq-1-1.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />

                                <Image alt='faq-1-2' src={'/faq/faq-1-2.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />
                            </Stack>
                            <Box mb='30px' />
                            <Text textAlign={'justify'} size={'lg'} textColor=''>To reverse the direction of rotation of a single-phase asynchronous motor, the position of the copper plates and wires must be changed:</Text>
                            <Box mb='30px' />
                            <Stack justifyContent={{ xs: 'center', md: 'left' }} direction={stackDirection} spacing={{ xs: 10, md: 40 }}>
                                <Image alt='faq-1-3' src={'/faq/faq-1-3.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />

                                <Image alt='faq-1-4' src={'/faq/faq-1-4.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />
                            </Stack>
                            <Box mb='30px' />
                            <Text textAlign={'justify'} size={'lg'} textColor=''>To reverse the direction of rotation of a single-phase EC motor, the direction selection input (brown wire) must be used:</Text>
                            <UnorderedList>
                                <ListItem>Connected to +12V =&gt; LG</ListItem>
                                <ListItem>Not connected =&gt; RD</ListItem>
                            </UnorderedList>


                        </AccordionPanel>
                    </AccordionItem>

                    {/* Accordion 2 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>Is it possible to change the position of my fan from LG to RD or vice versa?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>No, the impellers are different depending on the position (LG or RD), an inversion of impeller or volute will have a very strong impact on the flow rate obtained.</Text>
                            <Box mb='30px' />
                            <Stack justifyContent={{ xs: 'center', md: 'left' }} direction={stackDirection} spacing={{ xs: 10, md: 40 }}>
                                <Image alt='faq-2-1' src={'/faq/faq-2-1.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />

                                <Image alt='faq-2-2' src={'/faq/faq-2-2.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />
                            </Stack>
                        </AccordionPanel>
                    </AccordionItem>

                    {/* Accordion 3 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>How to install the motor in an enclosed pedestal?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>The motor must be installed inside the enclosed pedestal with the mounting brackets facing up.</Text>
                            <Box mb='30px' />
                            <Stack justifyContent={{ xs: 'center', md: 'left' }} direction={stackDirection} spacing={{ xs: 10, md: 40 }}>
                                <Image alt='faq-3-1' src={'/faq/faq-3-1.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />

                            </Stack>
                            <Box mb='30px' />
                            <Text textAlign={'justify'} size={'lg'} textColor=''>We adjust the orientation of the motor to the type of pedestal.</Text>
                        </AccordionPanel>
                    </AccordionItem>


                    {/* Accordion 4 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>The SEAT50 fan does not reach its rated speed and trips the electrical protections?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>Check the direction of rotation of the motor. This fan is only available in the LG position.</Text>
                        </AccordionPanel>
                    </AccordionItem>



                    {/* Accordion 5 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>Is it possible to use a three-phase motor on a single-phase 230V installation?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>Yes, it is necessary to use a VFD with a single phase 230V supply, this VFD will create three 230V phases generally connected in a delta configuration on the motor (refer to the fan installation manual).</Text>
                        </AccordionPanel>
                    </AccordionItem>

                    {/* Accordion 6 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>How to connect a three-phase asynchronous motor?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>Consult the nameplate and find out about your electrical installation. If you are using a 230V frequency converter, you should generally use the delta connection and if you have a 400V three-phase supply or a 400V VFD, you should generally use the star connection.</Text>

                            <Box mb='20px' />
                            <Flex direction={'horizontal'} spacing={40} justifyContent={'center'}>
                                <Image alt='faq-table' src={'/faq/table-faq.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />
                            </Flex>
                            <Box mb='30px' />
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>The smaller of the two voltages corresponds to a delta circuit.</Text>
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>Caution! For 400/690 motors the delta connection must be used with 400V.</Text>
                            <Box mb='30px' />
                            <Stack justifyContent={{ xs: 'center', md: 'left' }} direction={stackDirection} spacing={{ xs: 10, md: 40 }}>
                                <Image alt='faq-6-1' src={'/faq/faq-6-1.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />

                                <Image alt='faq-6-2' src={'/faq/faq-6-2.webp'} objectFit={'cover'} w={{ xs: 200, md: 300 }} />
                            </Stack>

                        </AccordionPanel>
                    </AccordionItem>



                    {/* Accordion 7 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>How to calculate the pressure drop in a ventilation system?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>Many parameters (diameter, type, equipment, filters, etc.) influence pressure drops, so do not hesitate to contact us to define the characteristics of your installation together. SEAT VENTILATION also provides training for you and your teams.</Text>

                        </AccordionPanel>
                    </AccordionItem>


                    {/* Accordion 8 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>Is it necessary to include a purge?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>The volute must not fill with liquid, if the position of the fan can allow rainwater to enter the fan, a drain must be provided and specified when ordering. Attention must also be paid to the condensation that may occur in the ductwork.</Text>

                        </AccordionPanel>
                    </AccordionItem>


                    {/* Accordion 9 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>What is the operating temperature range of the fans?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>The fans can be used between -15°C and +60°C.</Text>

                        </AccordionPanel>
                    </AccordionItem>


                    {/* Accordion 10 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>What is the compatibility with chemicals?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>The following is a non-exhaustive list of products compatible with SEAT fans at 20°C :</Text>
                            <Stack justifyContent={{ xs: 'center', md: 'left' }} direction={stackDirection} spacing={{ xs: 10, md: 40 }}>
                                <UnorderedList>
                                    <ListItem>Acetone</ListItem>
                                    <ListItem>Acetonitrile</ListItem>
                                    <ListItem>Acetic acid</ListItem>
                                    <ListItem>Acetic acid, glacial</ListItem>
                                    <ListItem>Boric acid</ListItem>
                                    <ListItem>Hydrobromic acid</ListItem>
                                    <ListItem>Hydrochloric acid 37%</ListItem>
                                    <ListItem>Hydrofluoric acid concentrate</ListItem>
                                    <ListItem>100% Methanol</ListItem>
                                    <ListItem>100% Ethanol</ListItem>
                                    <ListItem>100% Isopropanol</ListItem>
                                </UnorderedList>

                                <UnorderedList>
                                    <ListItem>Alums</ListItem>
                                    <ListItem>Ammonia aq. 25%</ListItem>
                                    <ListItem>Chloroform</ListItem>
                                    <ListItem>H2O2 30% ETHYLENE DIAMINE</ListItem>
                                    <ListItem>Ethylene diamine</ListItem>
                                    <ListItem>Ethylene glycol</ListItem>
                                    <ListItem>Formaldehyde 37%</ListItem>
                                    <ListItem>Hydroquinone</ListItem>
                                    <ListItem>NaOH 10N</ListItem>
                                    <ListItem>Pyridine</ListItem>
                                </UnorderedList>
                            </Stack>
                            <Box mb='20px' />
                            <Text mb='10px' textAlign={'justify'} size={'lg'} textColor=''>The following is a non-exhaustive list of products NOT compatible with SEAT fans :</Text>
                            <UnorderedList>
                                <ListItem>Concentrated nitric acid</ListItem>
                                <ListItem>Concentrated sulphuric acid 100%</ListItem>
                                <ListItem>Bromine</ListItem>
                                <ListItem>Diethyl ether</ListItem>
                                <ListItem>Phenol</ListItem>
                                <ListItem>Triclorethylene</ListItem>
                            </UnorderedList>
                            <Box mb='20px' />
                            <Text textAlign={'justify'} size={'lg'} textColor=''>Do not hesitate to consult us for any other product, the compatibility also depends on the temperature and the concentration of the product.</Text>

                        </AccordionPanel>
                    </AccordionItem>


                    {/* Accordion 11 */}
                    <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>What is the warranty period?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>The fans are warranted against defects in material and workmanship for two years from the date of original shipment. Any unit or part found to be defective and reported within the warranty period will be replaced after a survey by SEAT VENTILATION, freight prepaid by the shipper. Wear and tear due to heat, abrasive action, chemicals, incorrect installation or use or lack of normal maintenance are not defects and are not covered by the warranty. Transportation to and from the factory for warranty repairs is not covered by the warranty and is the sole responsibility of the equipment owner.
                                The manufacturer will not be responsible for the cost of installation, removal, or reinstallation or for any consequential damages resulting from failure to meet the terms of any warranty.</Text>

                        </AccordionPanel>
                    </AccordionItem>


                    {/* Accordion 12 */}
                    {/* <AccordionItem pb={6} border={'none'}>
                        <h2>
                            <AccordionButton>
                                <Box as='span' flex='1' textAlign='left'>
                                    <HStack>
                                        <FaQuestionCircle />
                                        <Heading size={'sm'}>Which fan to choose?</Heading>
                                    </HStack>
                                </Box>
                                <AccordionIcon />
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                            <Text textAlign={'justify'} size={'lg'} textColor=''>Which fan to choose?</Text>

                        </AccordionPanel>
                    </AccordionItem> */}


                </Accordion>
            </Box>

            <Footer />
            {/* <ShowDescriptionButton /> */}
        </Box>
    )

}
