import React, { useState } from "react";
import { Flex, Box, Text, Image, Center, UnorderedList, ListItem, Link, Button, Icon } from "@chakra-ui/react";
import { FiChevronRight } from 'react-icons/fi'; // Importing the right arrow icon

const CardProduct = ({ title, image, description, url, page, products }) => {
  const [isFocused, setIsFocused] = useState(false);

  const handleImageFocus = () => {
    setIsFocused(!isFocused);
  };

  return (
    <Flex direction="column" maxW="sm" borderRadius="lg" overflow="hidden" m="4">
      <Box>
        <Center>
          <Text fontSize="lg" fontWeight="bold">
            {title}
          </Text>
        </Center>
        <Center onClick={handleImageFocus}>
          <Image
            src={image}
            alt={title}
            w={isFocused ? '100%' : '240px'}
            my='20px'
            transition="width 0.5s"
            cursor="pointer"
            _hover={{ opacity: 0.8 }}
          />
        </Center>
        <Box p="6">
          <Text mt="2" color="gray.600" textAlign={"justify"} textColor={'blackSeat.500'}>
            {description}
          </Text>

          <Box mb='10px' />
          <UnorderedList key={{ title }}>
            {products.map((item) => (
              <Link key={item.name} _hover={{ color: "redSeat.400" }}>
                <ListItem key={item.name}>{item.name}</ListItem>
              </Link>
            ))}
          </UnorderedList>
        </Box>
      </Box>
      <Flex justify="center" mt="auto">
        <Button
          size={'lg'}
          variant="outline"
          color='redSeat.700'
          borderColor="redSeat.700"
          bg="transparent"

          _hover={{ bg: "redSeat.700", color: "white" }}
          rightIcon={<Icon as={FiChevronRight} />}
        >
          <Link href={page} _hover={{ color: "white" }} textDecoration='none'>
            {title}
          </Link>
        </Button>
      </Flex>
    </Flex>
  );
};

const handleButtonClick = (url) => {
  import('react-dom').then((ReactDOM) => {
    window.open(url);
  });
};

export default CardProduct;
