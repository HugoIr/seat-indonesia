import { Box, Icon } from "@chakra-ui/react";
import { FaMapMarkerAlt } from "react-icons/fa";
const MapWithMarker = () => {
    return (
        // <iframe width="100%" height="550" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;t=m&amp;daddr=-6.177167,106.714861&amp;spn=0.018486,0.027466&amp;z=17&amp;output=embed"></iframe>
        <Box position="relative" width="100%" height="400px" bg="blue.200" borderRadius="md" overflow="hidden">
            <iframe
                title="Google Map"
                src="https://maps.google.com/?ie=UTF8&amp;t=m&amp;daddr=-6.177167,106.714861&amp;spn=0.018486,0.027466&amp;z=17&amp;output=embed"
                width="100%"
                height="100%"
                style={{ border: 0 }}
                allowFullScreen=""
                loading="lazy"
            ></iframe>

            {/* <div
                style={{
                    position: "absolute",
                    top: "45%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    width: "40px",
                    height: "40px",
                    borderRadius: "50%",
                    zIndex: 1,
                }}

            ><Icon as={FaMapMarkerAlt} color='redSeat.400' w={'40px'} h={'40px'} /></div> */}
        </Box>
    );
};

export default MapWithMarker;
