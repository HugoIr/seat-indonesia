import React, { useState } from "react";
import { motion } from 'framer-motion';
import { Box } from '@chakra-ui/react';

const GifWithTransition = ({ src, alt, width = '100%', text, isShowed }) => {
    return (
        < >
            <motion.div
                // whileHover={{ opacity: 1 }} // Increase opacity on hover
                whileTap={{ scale: 0.9 }} // Decrease scale on tap
                style={{ opacity: isShowed ? 1 : 0, transition: 'opacity 0.5s ease-in-out' }} // Initially hidden
            // transform="translateX(-50%)"
            >
                <Box borderRadius={200} bg='white'>
                    <motion.img
                        src={src}
                        alt={alt}
                        style={{ width: width, height: 'auto' }}
                    />
                </Box>

            </motion.div>

        </>
    );
};

export default GifWithTransition;
