import { Box, Text, TextProps } from '@chakra-ui/layout';
import { HStack, Image } from '@chakra-ui/react';
import { FC } from 'react';

const Logo = (props) => {
  const { inlineSize = '124px', ml = '20px', src = '/logo/logo-white.png', ...rest } = props;
  return (
    <HStack inlineSize={inlineSize} ml={ml} >
      <Image alt='logo-seat' src={src} objectFit={'cover'} />
    </HStack>
  );
};

export default Logo;
