import React, { useState, useEffect } from 'react';
import { Box, Text } from '@chakra-ui/react';

const ShowDescriptionOnScroll = () => {
    const [showDescription, setShowDescription] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            // Calculate the distance scrolled from the top of the document
            const scrollY = window.scrollY || window.pageYOffset;
            // Define a threshold value where the description should be shown
            const threshold = 500; // Adjust as needed
            setShowDescription(scrollY > threshold);
        };

        // Add scroll event listener
        window.addEventListener('scroll', handleScroll);

        // Clean up event listener on component unmount
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <Box textAlign="center" py={6} position="relative">
            {/* {showDescription && (
                <Box
                    mt={4}
                    bg="white"
                    borderRadius="md"
                    boxShadow="md"
                    p={4}
                    position="fixed"
                    top="50px"
                    left="50%"
                    transform="translateX(-50%)"
                    zIndex="2"
                >
                    <Text fontSize="lg">
                        This is the description of the feature. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Integer posuere erat a ante venenatis,
                        quis interdum neque aliquam.
                    </Text>
                </Box>
            )} */}
            <Box
                position="fixed"
                top={showDescription ? '50px' : '-50px'} // Move arrow below/above description
                left="50%"
                transform="translateX(-50%)"
                width="2px"
                height="10vh"
                bg="black"
                transition="top 0.5s"
                zIndex="1"
            >
                <Box
                    mt={4}
                    bg="white"
                    borderRadius="md"
                    boxShadow="md"
                    p={4}
                    position="fixed"
                    top="50px"
                    left="50%"
                    transform="translateX(-50%)"
                    zIndex="2"
                >
                    <Text fontSize="lg">
                        This is the description of the feature. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Integer posuere erat a ante venenatis,
                        quis interdum neque aliquam.
                    </Text>
                </Box>
            </Box>
        </Box>
    );
};

export default ShowDescriptionOnScroll;
