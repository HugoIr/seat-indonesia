import React from "react";
import { Box, Center, Flex, Heading, Text } from "@chakra-ui/react";
import CardProduct from '../Elements/CardProduct';
import FadeInCard from '../FadeInCard';
const SectionProduct = () => {
    const data = [
        {
            title: "SEAT Series",
            image: "/product/seat/seat-green.png",
            description: "Made of polypropylene, a corrosion resistant material. Used in laboratories, Fume Hood (Lemari Asam), and most industrial extractions. Simple to install, small in size and contain a good flow/pressure ratio.",
            url: 'https://www.seat-ventilation.com/products/by-category/seat-series/',
            page: 'products/seat-series',
            products: [
                { name: 'SEAT 15', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-15' },
                { name: 'SEAT 20', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-20' },
                { name: 'SEAT 25', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-25' },
                { name: 'SEAT 30', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-30' },
                { name: 'SEAT 35', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-35' },
                { name: 'SEAT 50', url: 'https://www.seat-ventilation.com/products/by-category/seat-series/seat-50' },
            ]
        },
        {
            title: "STORM Series",
            image: "/product/storm/storm-pink.png",
            description: "High pressure fans for applications such as storage cabinets, exhaust arms, washing towers, collectors, Fume Hood (Lemari Asam), or filter boxes requiring high static pressure, and with high pressure drops.",
            url: 'https://www.seat-ventilation.com/products/by-category/storm-series/',
            page: 'products/storm-series',
            products: [
                { name: 'STORM 10', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-10' },
                { name: 'STORM 12', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-12' },
                { name: 'STORM 14', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-14' },
                { name: 'STORM 16', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-16' },
                { name: 'STORM 18', url: 'https://www.seat-ventilation.com/products/by-category/storm-series/storm-18' },
            ]
        },
        {
            title: "JET Series",
            image: "/product/jet/jet-yellow.png",
            description: "Incorporate the in-line assembly method. The motor is protected from the corrosive flow and the weather inside the cone. With vertical discharge, these fans are usually roof mounted. Casing and Impeller from Plastic material (Polypropylene).",
            url: 'https://www.seat-ventilation.com/products/by-category/jet-series/',
            page: 'products/jet-series',
            products: [
                { name: 'JET 20', url: 'https://www.seat-ventilation.com/products/by-category/jet-series/jet-20' },
                { name: 'JET 25', url: 'https://www.seat-ventilation.com/products/by-category/jet-series/jet-25' },
                { name: 'JET 30', url: 'https://www.seat-ventilation.com/products/by-category/jet-series/jet-30' },

            ]
        },
    ];

    return (
        <Box mt={16}>
            <Box textAlign={'center'}>
                <Heading as='h4' color={'redSeat.700'}>Our Products</Heading>
                <Box mb='20px' />
                <Box mx={'16vw'}>
                    <Text fontSize={'lg'} >SEAT Ventilation INDONESIA, We are a sole agent of SEAT, STORM, JET Series in Indonesian market. SEAT Products are High quality. SEAT Products are competitive price. Applied to Fumehood (Lemari Asam), Water Treatment Plant, Chemical Air, Explosion Proof Area, and others.</Text>
                </Box>
            </Box >

            <Box mb={'42px'} />
            <Flex flexWrap="wrap" alignItems='stretch' justifyContent="center">
                {data.map((card, index) => (
                    <FadeInCard key={index}>
                        <CardProduct key={index} {...card} />
                    </FadeInCard>
                ))}
            </Flex>
            <Box mb='4rem' />
        </Box>
    );
};

export default SectionProduct;