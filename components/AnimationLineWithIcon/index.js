import React, { useRef, useEffect, useState } from 'react';
import { Box, ChakraProvider, HStack, Image } from '@chakra-ui/react';
import { keyframes } from '@emotion/react';

// const lineAnimation = keyframes`
//   0% { transform: scaleX(0); }
//   100% { transform: scaleX(0.96); }
// `;
const lineAnimation = keyframes`
  0% { transform: scaleX(0); }
  100% { transform: scaleX(0.92); }
`;

// const imageAnimation = keyframes`
//   0% { transform: translateX(2vw) scale(0.5); }
//   100% { transform: translateX(73vw) scale(2); }
// `;

const imageAnimation = keyframes`
  0% { transform: translateX(4vw) scale(0.7); }
  100% { transform: translateX(70vw) scale(2); }
`;

const imageSpinAnimation = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;


const AnimatedLineWithIcon = () => {
    const [isInView, setIsInView] = useState(false);
    const containerRef = useRef(null);

    useEffect(() => {
        const observer = new IntersectionObserver(
            ([entry]) => {
                if (entry.isIntersecting) {
                    setIsInView(true);
                    observer.disconnect();
                }
            },
            {
                threshold: 0.1,
            }
        );

        if (containerRef.current) {
            observer.observe(containerRef.current);
        }

        return () => {
            if (observer && observer.disconnect) {
                observer.disconnect();
            }
        };
    }, []);

    return (
        <ChakraProvider>
            <HStack
                ref={containerRef}
                position="relative"
                width='100%'
                pl='4vw'
                height="30px" // Adjusted height for better visibility of the animation
                overflow="hidden"
                transformOrigin="left"
            // animation={isInView ? `${lineAnimation} 10s ease-in-out forwards` : 'none'}
            >
                <Box
                    height="2px"
                    bg="red.800"
                    width="100%"
                    transformOrigin="left"
                    animation={isInView ? `${lineAnimation} 10s ease-in-out forwards` : 'none'}
                />
                <Box
                    animation={isInView ? `${imageAnimation} 10s ease-in-out forwards` : 'none'}
                    style={{ position: 'absolute', left: '0', transform: 'translateY(-50%)' }} // Position the image at the start and center vertically
                >
                    <Image
                        src={'fan.gif'}
                        width='20px'
                        transformOrigin="center"
                        animation={isInView ? `${imageSpinAnimation} 2s linear infinite` : 'none'}

                    // animation={isInView ? `${imageAnimation} 3s ease-in-out forwards, ${imageSpinAnimation} 3s linear infinite` : 'none'}
                    // style={{ position: 'absolute', left: '0', transform: 'translateY(-50%)' }} // Position the image at the start and center vertically
                    />

                </Box>
            </HStack>
        </ChakraProvider>
    );
};

export default AnimatedLineWithIcon;
