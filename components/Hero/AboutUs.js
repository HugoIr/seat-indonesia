import { Box, Image } from '@chakra-ui/react';
import React from 'react';

const HeroAboutUs = () => {
    return (
        <Box position="relative">
            <Image
                src="/about-us/SEAT Image - About Us.webp"
                _placeholder="/about-us/placeholder/SEAT Image - About Us.webp"
                // src='/about-us/carousel-size.png'
                alt={`SEAT - About Us`}
                width='100%'
                height="60%"
                objectFit={'contain'}
            />
            {/* <Flex
                height="60%" // Set the minimum height to the full viewport height
                width="100%"
                backgroundImage="/about-us/SEAT Image - About Us.webp"
                _placeholder="/about-us/placeholder/lab-research.png"
                backgroundSize="cover"
                backgroundPosition="center"
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                color="white"
            >
            </Flex> */}
        </Box>
    );
};

export default HeroAboutUs;
