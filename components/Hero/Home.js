import React, { useState, useEffect } from "react";
import { Box, Heading, Center, Image, HStack, Flex, Text, ScaleFade } from '@chakra-ui/react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // Import carousel styles
import Logo from '../Elements/Logo';

const HeroHome = () => {
    // const [showLogo, setShowLogo] = useState(false);
    const [scrollOffset, setScrollOffset] = useState(0);
    // useEffect(() => {
    //     const handleScroll = () => {
    //         setScrollOffset(window.pageYOffset);
    //     };

    //     window.addEventListener("scroll", handleScroll);

    //     return () => {
    //         window.removeEventListener("scroll", handleScroll);
    //     };
    // }, []);

    // useEffect(() => {
    //     setShowLogo(scrollOffset > 20);
    // }, [scrollOffset]);
    return (
        <Box position="relative">
            <Carousel
                showArrows={true}
                showThumbs={false}
                showStatus={false}
                infiniteLoop={true}
                autoPlay={true}
                interval={5000}
                swipeScrollTolerance={40}
                preventMovementUntilSwipeScrollTolerance={true}
                // style={{ height: "80vh" }} // Set carousel height to 100vh
                key='carousel'
            >
                {[1, 2, 3, 4, 5].map(item => (
                    <Box key={item}>
                        <Box
                            key={item}
                        // background={`linear-gradient(to top, rgba(255, 255, 255, 1), transparent)`}
                        >
                            {/* <Box bgColor="rgba(0, 0, 0, 0.75)"
                                height='100vh' position='absolute'
                                top="100%" // Position below the button
                                left="50%"
                            /> */}
                            <Image
                                src={`/carousel-home/carousel-${item}.webp`}
                                alt={`Carousel ${item}`}
                                width='100%'
                                height="60%"
                                objectFit={'contain'}
                                key={item}
                                _placeholder={`/carousel-home/placeholder/carousel-${item}.webp`}
                            />
                        </Box>

                        {/* <Box
                            position="absolute"
                            top="55%"
                            left="50%"
                            transform="translate(-50%, -50%)"
                            textAlign="center"
                            color="white"
                        >

                            <Center mb={8}>
                                <Logo src='/logo/logo-white.png' inlineSize={{ xs: '10rem', sm: '12rem', lg: '12rem' }} />
                            </Center>

                            <Box bg="rgba(0,0,0,0.25)" p={4} borderRadius="lg" inlineSize='50vw'>
                                <Heading as="h1" fontSize={{ xs: '20px', sm: '40px', md: '42px' }} fontWeight={600}>
                                    Regulation Solutions For Air Extraction In Laboratories And Chemical Air
                                </Heading>
                            </Box>
                        </Box > */}

                    </Box>
                )
                )}
            </Carousel>


        </Box >
    );
};

export default HeroHome;
