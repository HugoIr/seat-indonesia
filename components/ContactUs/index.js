import React from "react";
import { Grid, Box, Text, Link, Center, HStack, Icon, Heading } from "@chakra-ui/react";
import { FaBriefcase, FaFax, FaWhatsapp, FaEnvelope } from 'react-icons/fa';
import MapWithMarker from "../Elements/MapWithMarker";
import Logo from '../Elements/Logo';

const ContactUs = () => {
    const handleWhatsAppClick = (phoneNumber) => {
        import('react-dom').then((ReactDOM) => {
            const whatsappUrl = `https://wa.me/${phoneNumber}`;
            window.open(whatsappUrl, '_blank');
        });
    };
    const handleEmailClick = (emailAddress) => {
        import('react-dom').then((ReactDOM) => {
            window.location.href = `mailto:${emailAddress}`;
        });
    };
    return (
        <Box mx='8vw' mb='6rem'>
            <Center><Heading as='h6' textColor={'redSeat.700'}>Contact Us</Heading></Center>
            <Box mb='40px'></Box>
            <Grid
                templateColumns={{ base: "1fr", lg: "1fr 1fr" }}
                gap={{ base: "4", md: "6rem" }}
                padding="4"

            >
                {/* First Column: Google Maps Widget */}
                <Box>
                    <MapWithMarker />
                </Box>

                {/* Second Column: Address and Contact Info */}
                <Box margin="auto">
                    <Logo ml={0} inlineSize={{ sm: '20rem', md: '20rem' }} src='/ranescho/ranescho.png' />
                    <Box mb='16px' />
                    {/* Address */}
                    <Text fontSize="lg" fontWeight="bold" mb="2">
                        Address:
                    </Text>
                    <Box inlineSize='30rem'>
                        <Text mb="4">
                            Komplek Grand Palm Ruko CITY WALK Blok B No. 10
                            Jl Kresek Raya, Duri Kosambi, Cengkareng, Jakarta Barat 11750
                        </Text>
                    </Box>
                    {/* Contact Info */}
                    <Text fontSize="lg" fontWeight="bold" mb="2">
                        Contact:
                    </Text>
                    <Box mb="4" color="rgba(0,0,0, 0.75)">
                        <Box display="flex" alignItems="center" mb='8px'>
                            <Icon as={FaBriefcase} w={'20px'} h={'20px'} mr='6px' />
                            <Text as='span' fontWeight={600} color="black">Phone:&nbsp;</Text>
                            <Text as='span'>+ 62 21 29319906, 29319907, 22526412</Text>
                        </Box>
                        <Box display="flex" alignItems="center" mb='8px' >
                            <Icon as={FaWhatsapp} w={'20px'} h={'20px'} mr='6px' />
                            <Text as='span' fontWeight={600} color="black">Mobile:&nbsp;</Text>
                            <Text as='span' _hover={{ cursor: 'pointer', color: 'redSeat.400' }} onClick={() => handleWhatsAppClick(+6285132374373)}>+ 62 851 3237 4373</Text>
                        </Box>


                        <Box display="flex" alignItems="start" mb='8px'>
                            <Icon as={FaEnvelope} w={'20px'} h={'20px'} mr='6px' />
                            <Text as='span' fontWeight={600} color="black">Email:&nbsp;</Text>
                            <Text whiteSpace={'pre-line'} as='span'>
                                <Link
                                    to='#'
                                    onClick={(e) => {
                                        handleEmailClick('nesya.seatindonesia@gmail.com');
                                        e.preventDefault();
                                    }}
                                    _hover={{ color: 'redSeat.400' }}
                                >
                                    nesya.seatindonesia@gmail.com
                                </Link>
                                <pre />or&nbsp;

                                <Link
                                    to='#'
                                    onClick={(e) => {
                                        handleEmailClick('doddytan@ranescho.com');
                                        e.preventDefault();
                                    }}
                                    _hover={{ color: 'redSeat.400' }}
                                >
                                    doddytan@ranescho.com
                                </Link>
                            </Text>
                        </Box>

                    </Box>
                </Box >
            </Grid >
        </Box >
    );
};

export default ContactUs;
