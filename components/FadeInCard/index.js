import { Box, Text, Flex } from "@chakra-ui/react";
import { useEffect, useRef, useState } from "react";

const FadeInCard = ({ children }) => {
    const [isVisible, setIsVisible] = useState(false);
    const cardRef = useRef(null);

    useEffect(() => {
        const observer = new IntersectionObserver(
            (entries) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        setIsVisible(true);
                        observer.unobserve(entry.target);
                    }
                });
            },
            { threshold: 0.25 } // Adjust threshold as needed
        );

        if (cardRef.current) {
            observer.observe(cardRef.current);
        }

        return () => {
            if (cardRef.current) {
                observer.unobserve(cardRef.current);
            }
        };
    }, []);

    return (
        <Flex
            ref={cardRef}
            opacity={isVisible ? 1 : 0}
            // transition="opacity 0.5s ease-in-out"
            transform={`translateY(${isVisible ? 0 : "20px"})`}
            transition="opacity 0.5s ease, transform 0.5s ease"

        >
            {children}
        </Flex>
    );
};

export default FadeInCard;
