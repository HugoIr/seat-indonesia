import React from "react";
import { Flex, Box, Text, Link, Icon, HStack, Image } from "@chakra-ui/react";
import {
    FaFacebook,
    FaYoutube,
    FaLinkedin,
} from "react-icons/fa";

const Footer = () => {
    const handleWhatsAppClick = (phoneNumber) => {
        import('react-dom').then((ReactDOM) => {
            const whatsappUrl = `https://wa.me/${phoneNumber}`;
            window.open(whatsappUrl, '_blank');
        });
    };
    const handleEmailClick = (emailAddress) => {
        import('react-dom').then((ReactDOM) => {
            window.location.href = `mailto:${emailAddress}`;
        });
    };
    return (
        <Flex
            as="footer"
            align="center"
            justify="space-between"
            py='2rem'
            px={{ xs: '2.4rem', md: '4rem' }}
            bg="redSeat.700"
            direction={{ base: "column", md: "row" }}
        >
            {/* First Column */}
            <Box mb={{ xs: '30px', md: '0' }} mt={{ xs: '16px', md: '0' }} maxWidth='300px' mr='20px' >
                <Box mb="4">
                    <HStack inlineSize={'160px'} >
                        <Image alt='logo-seat-white' src='/logo/logo-white.png' objectFit={'cover'} />
                    </HStack>
                    <Box mb='16px' />
                    <Box >
                        <Text color="rgba(255, 255, 255, 0.75)">
                            Regulation Solutions For Air Extraction In Laboratories And Chemical Air
                        </Text>
                    </Box>
                </Box>
                <Box>
                    {/* Social Media Icons */}
                    <Link target='_blank' href="https://web.facebook.com/seatventilation/" mr="2">
                        <Icon as={FaFacebook} boxSize={6} color="rgba(255, 255, 255, 0.75)" />
                    </Link>
                    <Link target='_blank' href="https://fr.linkedin.com/company/seatventilation" mr="2">
                        <Icon as={FaLinkedin} boxSize={6} color="rgba(255, 255, 255, 0.75)" />
                    </Link>
                    <Link target='_blank' href="https://www.youtube.com/channel/UCQNl2GHWZDC1s6ZCb3KTwjA">
                        <Icon as={FaYoutube} boxSize={6} color="rgba(255, 255, 255, 0.75)" />
                    </Link>
                </Box>
            </Box>

            {/* Second Column */}
            <Box>
                <Box mb="4">
                    <Text textColor={'white'} mb={'8px'}>
                        Address
                    </Text>
                    <Box inlineSize={'300px'}>
                        <Text color="rgba(255, 255, 255, 0.75)">
                            Komplek Grand Palm Ruko CITY WALK Blok B No. 10
                            Jl Kresek Raya, Duri Kosambi, Cengkareng, Jakarta Barat 11750
                        </Text>
                    </Box>
                </Box>
            </Box>

            {/* Third Column */}
            <Box alignSelf={{ xs: 'center', md: 'start' }} mt='20px' width='300px'>
                <Box mb="4">
                    <Text textColor={'white'} mb={'8px'}>
                        Contact
                    </Text>
                    <Box display='inline-block'>
                        <Text as='span' color="white">Phone: </Text>
                        <Text as='span' color="rgba(255, 255, 255, 0.75)" wordBreak='break-word'>+ 62 21 29319906, 29319907, 22526412</Text>
                    </Box>
                    <Box >
                        <Text as='span' color="white">Mobile: </Text>
                        <Text as='span' color="rgba(255, 255, 255, 0.75)" _hover={{ color: 'redSeat.400', cursor: 'pointer' }} onClick={() => handleWhatsAppClick(+6285132374373)}>+ 62 851 3237 4373</Text>
                    </Box>
                    <Box>
                        <Text as='span' color="white">Email: </Text>
                        <Text whiteSpace={'pre-line'} as='span' color="rgba(255, 255, 255, 0.75)">
                            <Link
                                to='#'
                                onClick={(e) => {
                                    handleEmailClick('nesya.seatindonesia@gmail.com');
                                    e.preventDefault();
                                }}
                                _hover={{ color: 'redSeat.400' }}
                            >
                                nesya.seatindonesia@gmail.com
                            </Link>
                            <pre />or&nbsp;

                            <Link
                                to='#'
                                onClick={(e) => {
                                    handleEmailClick('doddytan@ranescho.com');
                                    e.preventDefault();
                                }}
                                _hover={{ color: 'redSeat.400' }}
                            >
                                doddytan@ranescho.com
                            </Link>


                        </Text>
                    </Box>
                </Box>
            </Box>
        </Flex>
    );
};

export default Footer;
