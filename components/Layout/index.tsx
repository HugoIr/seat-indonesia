import { Box } from '@chakra-ui/layout';
import { FC } from 'react';
import Navbar from './Navbar.js';

import React, { ReactNode } from 'react';

interface LayoutProps {
  children: ReactNode; // Use ReactNode type for children
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <Box minH="100vh">
      <Navbar />
      {children}
    </Box>
  );
};

export default Layout;
