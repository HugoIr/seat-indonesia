import React, { useState, useEffect } from "react";
import {
  Box,
  Flex,
  Text,
  IconButton,
  SlideFade,
  Spacer,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerHeader,
  DrawerBody,
  Link,
  Icon,
  VStack,
  HStack,
} from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import Logo from "../Elements/Logo";
import { useRouter } from 'next/router';
import GifWithTransition from '../Elements/GifWithTransition';
import { FaWhatsapp } from "react-icons/fa";

const Navbar = () => {
  // const [showLogo, setShowLogo] = useState(false);
  // const [scrollOffset, setScrollOffset] = useState(0);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const router = useRouter();
  const currentPath = router.pathname;

  const handleWhatsAppClick = (phoneNumber) => {
    import('react-dom').then((ReactDOM) => {
      const whatsappUrl = `https://wa.me/${phoneNumber}`;
      window.open(whatsappUrl, '_blank');
    });
  };

  // useEffect(() => {
  //   const handleScroll = () => {
  //     setScrollOffset(window.pageYOffset);
  //   };

  //   window.addEventListener("scroll", handleScroll);

  //   return () => {
  //     window.removeEventListener("scroll", handleScroll);
  //   };
  // }, []);

  // useEffect(() => {
  //   setShowLogo(scrollOffset > 20);
  // }, [scrollOffset]);

  return (
    <Box>


      <Flex
        as="nav"
        align="center"
        justify="space-between"
        // padding="0.4rem"
        // bgColor={(currentPath == '/about-us' ? 'rgba(0, 0, 0, 0.1)' : 'rgba(0, 0, 0, 0.2)')}
        bgColor={'#f1f1f1'}
        color={'redSeat.700'}
        position="sticky"
        top="0"
        left="0"
        right="0"
        zIndex="999"
        width="100%"
      >
        <VStack w='full'>
          <HStack w='full' justifyContent="space-between" mt='6px'>
            <Box display="flex" alignItems="center" ml='2rem' >
              <Icon as={FaWhatsapp} color='green' w={'20px'} h={'20px'} mr='6px' />
              <Text as='span' fontSize='14px' fontWeight={500} color="rgba(0, 0, 0, 0.85)">Mobile:&nbsp;</Text>
              <Text as='span' fontSize='14px' _hover={{ cursor: 'pointer', color: 'redSeat.400' }} color="rgba(0, 0, 0, 0.75)" onClick={() => handleWhatsAppClick(+6285132374373)}>+ 62 851 3237 4373</Text>
            </Box>
            <Box w='4%' />
          </HStack>


          <HStack w='full' justifyContent="space-between" bgColor='white' py='0.6rem' >
            <Box >

              <Link href="/">
                <Logo src='/logo/logo.png' />
              </Link>
            </Box>
            <Spacer />

            {/* Menu Items */}
            <Flex align="center">
              {/* Hamburger Icon */}
              <IconButton
                aria-label="Menu"
                icon={<HamburgerIcon />}
                bgColor='white'
                _hover={{ bgColor: 'redSeat.700', color: 'white' }}
                variant="ghost"
                onClick={() => setIsMobileMenuOpen(true)}
                display={{ base: "block", md: "none" }}
              />

              {/* Other Menu Items */}
              <Box mr={4} display={{ base: "none", md: "block" }}>
                <Text fontSize="md" fontWeight="semiBold" >
                  <Link href='/' _hover={{ color: "redSeat.400" }}>
                    Home
                  </Link>
                </Text>
              </Box>
              <Box mr={4} display={{ base: "none", md: "block" }}>
                <ProductsMenu />
              </Box>

              <Box mr={4} display={{ base: "none", md: "block" }}>
                <Text fontSize="md" fontWeight="semiBold" >
                  <Link href='/gallery' _hover={{ color: "redSeat.400" }}>
                    Gallery
                  </Link>
                </Text>
              </Box>

              <Box mr={4} display={{ base: "none", md: "block" }}>
                <SolutionsMenu />
              </Box>
              <Box mr={4} display={{ base: "none", md: "block" }}>
                <Text fontSize="md" fontWeight="semiBold" >
                  <Link href='/faq' _hover={{ color: "redSeat.400" }}>
                    F.A.Q
                  </Link>
                </Text>
              </Box>
              <Box mr={10} display={{ base: "none", md: "block" }}>
                <Text fontSize="md" fontWeight="semiBold" >
                  <Link href='/about-us' _hover={{ color: "redSeat.400" }}>
                    About Us
                  </Link>
                </Text>
              </Box>


            </Flex>

            {/* Mobile Menu Drawer */}
            <MobileMenuDrawer
              isOpen={isMobileMenuOpen}
              onClose={() => setIsMobileMenuOpen(false)}
            />
          </HStack>
        </VStack>
      </Flex>
    </Box>
  );
};
const SolutionsMenu = () => {
  const [indexHover, setIndexHover] = useState(-1);
  const [isOpen, setIsOpen] = useState(false);
  let timeoutId;

  const handleMouseEnter = () => {
    setIsOpen(true);
    clearTimeout(timeoutId);
  };

  const handleMouseLeave = () => {
    timeoutId = setTimeout(() => {
      setIsOpen(false);
    }, 750);
  };

  const handleMenuListMouseEnter = () => {
    clearTimeout(timeoutId);
  };

  const handleMenuListMouseLeave = () => {
    // setIsOpen(false);
    timeoutId = setTimeout(() => {
      setIsOpen(false);
    }, 750);
  };
  return (
    <Menu isOpen={isOpen}>
      <MenuButton
        onClick={handleMouseEnter}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        _hover={{ color: "redSeat.400" }}
        transition="background-color 0.3s ease"
      >
        <Text fontSize="md" fontWeight="semiBold" _hover={{ cursor: "pointer", textColor: "redSeat.400" }} >
          Solution
        </Text>
      </MenuButton>
      <MenuList
        bgColor={'white'}
        _hover={{ cursor: "pointer", textColor: "redSeat.400", color: 'white' }}
        onMouseEnter={handleMenuListMouseEnter}
        onMouseLeave={handleMenuListMouseLeave}
      >
        <MenuItem
          textColor={'blackSeat.500'}
          onMouseEnter={() => setIndexHover(0)}
          onMouseLeave={() => setIndexHover(-1)}
          _hover={{ bg: 'white', color: 'redSeat.700' }}
        >
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan 1' width="30px" isShowed={indexHover == 0} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/solutions/fume-hood">
            <Text ml='8px'>Fume hood maker</Text>
          </Link>
        </MenuItem>
        <MenuItem textColor={'blackSeat.500'} onMouseEnter={() => setIndexHover(1)} onMouseLeave={() => setIndexHover(-1)} _hover={{ bg: 'white', color: 'redSeat.700' }}>
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan 2' width="30px" isShowed={indexHover == 1} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/solutions/chemical-laboratories">
            <Text ml='8px'>Chemical laboratories</Text>
          </Link>
        </MenuItem>
        <MenuItem textColor={'blackSeat.500'} onMouseEnter={() => setIndexHover(2)} onMouseLeave={() => setIndexHover(-1)} _hover={{ bg: 'white', color: 'redSeat.700' }}>
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan 3' width="30px" isShowed={indexHover == 2} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/solutions/hospital-solutions">

            <Text ml='8px'>Hospital solutions</Text>
          </Link>
        </MenuItem>
        <MenuItem textColor={'blackSeat.500'} onMouseEnter={() => setIndexHover(3)} onMouseLeave={() => setIndexHover(-1)} _hover={{ bg: 'white', color: 'redSeat.700' }}>
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan 4' width="30px" isShowed={indexHover == 3} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/solutions/chemical-industry">

            <Text ml='8px'>Pharmaceutical and<br />chemical industry</Text>
          </Link>
        </MenuItem>
        <MenuItem textColor={'blackSeat.500'} onMouseEnter={() => setIndexHover(4)} onMouseLeave={() => setIndexHover(-1)} _hover={{ bg: 'white', color: 'redSeat.700' }}>
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan 5' width="30px" isShowed={indexHover == 4} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/solutions/water-treatment">

            <Text ml='8px'>Water treatment</Text>
          </Link>
        </MenuItem>
      </MenuList>
    </Menu>
  );
};

const ProductsMenu = () => {
  const [indexHover, setIndexHover] = useState(-1);
  const [isOpen, setIsOpen] = useState(false);
  let timeoutId;

  const handleMouseEnter = () => {
    setIsOpen(true);
    clearTimeout(timeoutId);
  };

  const handleMouseLeave = () => {
    timeoutId = setTimeout(() => {
      setIsOpen(false);
    }, 750);
  };

  const handleMenuListMouseEnter = () => {
    clearTimeout(timeoutId);
  };

  const handleMenuListMouseLeave = () => {
    // setIsOpen(false);
    timeoutId = setTimeout(() => {
      setIsOpen(false);
    }, 750);
  };
  return (
    <Menu isOpen={isOpen}>
      <MenuButton
        onClick={handleMouseEnter}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        _hover={{ color: "redSeat.400" }}
        transition="background-color 0.3s ease"
      >
        <Text fontSize="md" fontWeight="semiBold" _hover={{ cursor: "pointer", textColor: "redSeat.400" }} >
          Product
        </Text>
      </MenuButton>
      <MenuList
        bgColor={'white'}
        _hover={{ cursor: "pointer", textColor: "redSeat.400", color: 'white' }}
        onMouseEnter={handleMenuListMouseEnter}
        onMouseLeave={handleMenuListMouseLeave}
      >
        <MenuItem
          textColor={'blackSeat.500'}
          onMouseEnter={() => setIndexHover(0)}
          onMouseLeave={() => setIndexHover(-1)}
          _hover={{ bg: 'white', color: 'redSeat.700' }}
        >
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan' width="30px" isShowed={indexHover == 0} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/products/seat-series">
            <Text ml='8px'>SEAT Series</Text>
          </Link>
        </MenuItem>
        <MenuItem textColor={'blackSeat.500'} onMouseEnter={() => setIndexHover(1)} onMouseLeave={() => setIndexHover(-1)} _hover={{ bg: 'white', color: 'redSeat.700' }}>
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan' width="30px" isShowed={indexHover == 1} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/products/storm-series">
            <Text ml='8px'>STORM Series</Text>
          </Link>
        </MenuItem>
        <MenuItem textColor={'blackSeat.500'} onMouseEnter={() => setIndexHover(2)} onMouseLeave={() => setIndexHover(-1)} _hover={{ bg: 'white', color: 'redSeat.700' }}>
          <Box ml='-30px'><GifWithTransition src='fan.gif' alt='Fan' width="30px" isShowed={indexHover == 2} /></Box>
          <Link _hover={{ cursor: "pointer", textColor: "redSeat.400" }} href="/products/jet-series">

            <Text ml='8px'>JET Series</Text>
          </Link>
        </MenuItem>
      </MenuList>
    </Menu>
  );
};


const MobileMenuDrawer = ({ isOpen, onClose }) => {
  return (
    <Drawer placement="right" onClose={onClose} isOpen={isOpen} size="full">
      <DrawerOverlay />
      <DrawerContent bg="redSeat.700" color="white">
        <DrawerHeader>
          <IconButton
            aria-label="Close"
            icon={<CloseIcon />}
            onClick={onClose}
            position="absolute"
            top="1rem"
            right="1rem"
          />
        </DrawerHeader>
        <DrawerBody>
          <Text fontSize="md" fontWeight="semiBold" mb={4} >
            <Link href='/' _hover={{ color: "redSeat.400" }}>
              Home
            </Link>
          </Text>


          <Text fontSize="md" fontWeight="semiBold" mb={4} >
            Products
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/products/seat-series"
              _hover={{ color: "redSeat.400" }}
            >
              SEAT Series
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/products/storm-series"
              _hover={{ color: "redSeat.400" }}
            >
              STORM Series
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/products/jet-series"
              _hover={{ color: "redSeat.400" }}
            >
              JET Series
            </Link>
          </Text>


          <Text fontSize="md" fontWeight="semiBold" mb={4} >
            <Link href='/gallery' _hover={{ color: "redSeat.400" }}>
              Gallery
            </Link>
          </Text>

          <Text fontSize="md" fontWeight="semiBold" mb={4} >
            Solutions
          </Text>



          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/solutions/fume-hood"
              _hover={{ color: "redSeat.400" }}
            >
              Fume hood maker
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/solutions/chemical-laboratories"
              _hover={{ color: "redSeat.400" }}
            >
              Chemical laboratories
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/solutions/hospital-solutions"
              _hover={{ color: "redSeat.400" }}
            >
              Hospital solutions
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/solutions/chemical-industry"
              _hover={{ color: "redSeat.400" }}
            >
              Pharmaceutical and chemical industry
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="400" mb={4} ml={4} >
            <Link
              href="/solutions/water-treatment"
              _hover={{ color: "redSeat.400" }}
            >
              Water treatment
            </Link>
          </Text>
          <Text fontSize="md" fontWeight="semiBold" mb={4} >
            <Link href='/faq' _hover={{ color: "redSeat.400" }}>
              F.A.Q
            </Link>
          </Text>

          <Text fontSize="md" fontWeight="semiBold" mb={4} >
            <Link href='/about-us' _hover={{ color: "redSeat.400" }}>
              About Us
            </Link>
          </Text>


        </DrawerBody>
      </DrawerContent>
    </Drawer>
  );
};

export default Navbar;
